class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.belongs_to :entity, polymorphic: true, index: true
      t.text :content, null: false

      t.timestamps null: false
    end
  end
end
