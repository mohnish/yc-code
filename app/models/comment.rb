class Comment < ActiveRecord::Base
  belongs_to :entity, polymorphic: true

  has_many :comments, as: :entity, :dependent => :delete_all

  alias_method :parent, :entity
end
