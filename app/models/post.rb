class Post < ActiveRecord::Base
  belongs_to :author, class_name: 'User', foreign_key: 'user_id'
  has_many :images
  has_many :comments, as: :entity

  validates_presence_of :author
  validates_presence_of :title
  validates_presence_of :content

end
