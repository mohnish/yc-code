class User < ActiveRecord::Base
  validates_presence_of :name
  validates_presence_of :city

  has_many :posts
end
