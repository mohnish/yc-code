json.array!(@comments) do |comment|
  json.extract! comment, :id, :content
  json.parent do
    parent = comment.parent
    json.id parent.id
    json.type parent.class.name
    json.content parent.content
  end
end
