json.extract! post, :id, :title, :content

json.author_name post.author.name

json.author_city post.author.city

json.images post.images do |image|
  json.extract! image, :id, :url
end
