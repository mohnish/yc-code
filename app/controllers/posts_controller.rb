class PostsController < ApplicationController
  before_action :set_post, only: [:show, :update, :destroy]

  def index
    # TODO: This could be passed in by the user. For now, default it to 10
    @posts = Post.last(10)
  end

  def show
    render json: {}, status: :not_found if @post.blank?
  end

  def create
    @post = Post.new(post_params)

    status =
      if @post.save
        :created
      else
        :unprocessable_entity
      end

    render status: status
  end

  def update
    status =
      if @post.update(post_params)
        :ok
      else
        :unprocessable_entity
      end

    render status: status
  end

  def destroy
    @post.destroy
    head :no_content
  end

  private
    def set_post
      @post = Post.find_by(id: params[:id])
    end

    def post_params
      hash = params.permit(:author_id, :title, :content)
      hash[:author] = User.find_by(id: hash.delete(:author_id)) if hash[:author_id].present?
      hash
    end
end
