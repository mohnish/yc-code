class ImagesController < ApplicationController
  before_action :set_image, only: [:show, :edit, :update, :destroy]

  def create
    @image = Image.new(image_params)

    status =
      if @image.save
        :created
      else
        :unprocessable_entity
      end

    render status: status
  end

  def destroy
    @image.destroy
    head :no_content
  end

  private
    def set_image
      @image = Image.find_by(id: params[:id])
    end

    def image_params
      params.permit(:post_id, :url)
    end
end
