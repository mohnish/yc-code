class CommentsController < ApplicationController
  before_action :set_comment, only: [:destroy]
  before_action :set_post, only: [:index, :create]

  def index
    @comments = @post.comments
  end

  def create
    @comment = @post.comments.create(comment_params)

    status =
      if @comment.save
        :created
      else
        :unprocessable_entity
      end

    render status: status
  end

  def destroy
    @comment.destroy
    head :no_content
  end

  private
    def set_comment
      @comment = Comment.find(params[:id])
    end

    def set_post
      @post = Post.find_by(id: params[:post_id])
    end

    def comment_params
      params.permit(:content)
    end
end
