# PostApp API

## Development

Before you continue, make sure you have the correct version of
Ruby installed on your machine. For Ruby installation information,
please refer to the `docs/installing_ruby.md` file.

from the application root, run the following commands
- `bin/setup` - sets up the app
- `bin/rspec` - run tests [not implemented yet]

## Running the app

from the application root, run
- `bin/rails s`

This will start the server @ [http://localhost:3000/](http://localhost:3000/)

If you had run the `bin/setup` earlier, you'll have access to
seed data and you can use the test users to create the posts.
Running `bin/rails c` followed by `User.all` will reveal the users in the database.
Pick one and use the endpoints to create the necessary data.

## API

### Posts

- list recent posts: `GET /posts/`
- view single post: `GET /posts/:id`
- create post: `POST /posts/`
payload:
```ruby
{ "title": "title", "content": "content", "author_id": "1" }
```
- update post: `PATCH /posts/:id`
payload:
```ruby
{ "title": "title", "content": "content" }
```
- delete post: `DELETE /posts/:id`

### Images

- create image: `POST /posts/:post_id/images`
payload:
```ruby
{ "url": "http://imgur.com/asdkjasd" }
```
- delete image: `PATCH /posts/:post_id/images/:id`

### Comments

- list a post's comments: `GET /posts/:post_id/comments`
- create comment: `POST /posts/:post_id/comments`
payload:
```ruby
{ "content": "title" }
```
- delete a comment/thread: `DELETE /comments/:id`

## Future work

- Implement edge case validations (updating nil records)
- Add support to add a comment to a comment through the API. Currently the public API for this functionality is not present (internally it's implemented)
- Add specs (make them pass)
- Add the reporting endpoint to show the user activity
